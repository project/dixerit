Description
------------------
Wraps the node body field in <!-- DIXERIT_START --> & <!-- DIXERIT_STOP --> tags to enable dixerit text to speech conversion.
Then adds a link to the mp3 file which is then embedded within a flash based player.

Author
-----------------
Paul Hart <paul.hart AT iriss DOT org DOT uk>